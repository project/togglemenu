(function ($) {
  Drupal.behaviors.toggleMenuInit = {
    init: function (selector, options) {
      if (options.burger === '') {
        delete options.burger;
      }
      $(selector).once('togglemenu').toggleMenu(options);
    },
    attach: function (context, settings) {
      var toggleMenu = this;
      var options = settings.togglemenu;
      $.each(Drupal.behaviors, function () {
        if ($.isPlainObject(this.togglemenuOptions)) {
          var togglemenuOptions = this.togglemenuOptions;

        } else if ($.isFunction(this.togglemenuOptions)) {
          var togglemenuOptions = this.togglemenuOptions();
        }
        $.extend(true, options, togglemenuOptions);
      });
      toggleMenu.init(settings.togglemenuSelector, options);
    }
  };
})(jQuery);
