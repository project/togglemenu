<?php

/**
 * @file
 * Drush integration for togglemenu.
 */

/**
 * The form_styler plugin URI.
 */
define('TOGGLEMENU_DOWNLOAD_URI', 'https://github.com/ArmGono/togglemenu/archive/master.zip');
define('TOGGLEMENU_DOWNLOAD_PREFIX', 'togglemenu-');

/**
 * Implements hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 */
function togglemenu_drush_command() {
  $items = array();

  // The key in the $items array is the name of the command.
  $items['togglemenu-plugin'] = array(
    'callback' => 'togglemenu_plugin',
    'description' => dt('Download and install the Toggle menu plugin.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'path' => dt('Optional. A path where to install the Toggle Menu plugin. If omitted Drush will use the default location.'),
    ),
    'aliases' => array('mtplugin'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 */
function togglemenu_drush_help($section) {
  switch ($section) {
    case 'drush:togglemenu-plugin':
      return dt('Download and install the Toggle Menu plugin from ArmGono/togglemenu, default location is sites/all/libraries.');
  }
}

/**
 * Implements drush_MODULE_pre_pm_enable().
 */
function drush_togglemenu_pre_pm_enable() {
  $modules = drush_get_context('PM_ENABLE_MODULES');
  if (in_array('togglemenu', $modules) && !drush_get_option('skip')) {
    togglemenu_plugin();
  }
}

/**
 * Command to download the Toggle Menu plugin.
 */
function togglemenu_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(TOGGLEMENU_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname = TOGGLEMENU_DOWNLOAD_PREFIX . basename($filepath, '.zip');
    // Remove any existing Toggle Menu plugin directory.
    if (is_dir($dirname) || is_dir('togglemenu')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('togglemenu', TRUE);
      drush_log(dt('A existing Toggle Menu plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename);

    // Change the directory name to "togglemenu" if needed.
    if ($dirname != 'togglemenu') {
      drush_move_dir($dirname, 'togglemenu', TRUE);
      $dirname = 'togglemenu';
    }
  }

  if (is_dir($dirname)) {
    drush_log(dt('Toggle Menu plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Toggle Menu plugin to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
